Feature: Login To HRM

  Scenario: Login with invalid credentials
    Given user is on login page
    When user enters the invalid username
    And user enter invalid password
    Then user should not be able to login

  Scenario: Login with valid credentials
    Given user is on login page
    When user enters the username
    And user enter password
    Then user should be able to login
