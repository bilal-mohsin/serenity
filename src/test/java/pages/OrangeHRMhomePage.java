package pages;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;

public class OrangeHRMhomePage extends PageObject {
	
	
	public void enterUsername(String username) {
		$(By.id("txtUsername")).type(username);
		
	}
	
	public void enterPassword(String password) {
		$(By.id("txtPassword")).type(password);
		
	}
	
	public void clickLogin() {
		$(By.id("btnLogin")).click();
		
	}
	
	public void invalidLogin() {
		$(By.id("spanMessage")).getText();
		
	}

	

}
