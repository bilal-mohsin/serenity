package pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class HomePage extends PageObject {

	OrangeHRMhomePage homePage;
	
	@Step
	public void openApplication() {
		homePage.open();
		
		
	}
	@Step
	public void enterUsername(String username) {
		homePage.enterUsername(username);
		
		
	}
	@Step
	public void enterPassword(String password) {
		
		homePage.enterPassword(password);
		
	}
	@Step
	public void clickOnLoginButton() {
		
		homePage.clickLogin();
		
	}
	
	@Step
	public void invalidMessage() {
		
		homePage.invalidLogin();;
		
	}
	
}
