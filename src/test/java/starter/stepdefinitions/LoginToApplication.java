package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.PageObject;
import pages.Dashboard;
import pages.HomePage;

public class LoginToApplication extends PageObject {
	
	@Steps
	HomePage home;
	
	@Steps
	Dashboard dash;
	
	@Given("user is on login page")
	public void user_is_on_login_page() {
	    home.openApplication();
	   
	}
	
	@When("user enters the invalid username")
	public void user_enters_the_invalid_username() {
		home.enterUsername("abc");
	}

	@When("user enter invalid password")
	public void user_enter_invalid_password() {
		home.enterPassword("bilal123");
		home.clickOnLoginButton();
	}
	@Then("user should not be able to login")
	public void user_should_not_be_able_to_login() {
		home.invalidMessage();
	}
			
	@When("user enters the username")
	public void user_enters_the_username() {
		home.enterUsername("Admin");
	}

	@When("user enter password")
	public void user_enter_password() {
		home.enterPassword("admin123");
		home.clickOnLoginButton();
	}
	@Then("user should be able to login")
	public void user_should_be_able_to_login() {
		dash.verifyAdmin();
	    
	}
	

	

}
